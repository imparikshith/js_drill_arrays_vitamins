function orderByVitamins(vitamin) {
  if (vitamin.includes("Vitamin A")) {
    return 1;
  } else if (vitamin.includes("Vitamin B")) {
    return 2;
  } else if (vitamin.includes("Vitamin C")) {
    return 3;
  } else if (vitamin.includes("Vitamin D")) {
    return 4;
  } else if (vitamin.includes("Vitamin K")) {
    return 5;
  } else {
    return null;
  }
}
function sortByVitamins(items) {
  const sortedItems = items
    .sort(function (a, b) {
      if (orderByVitamins(a.contains) == orderByVitamins(b.contains)) {
        let vitamin;
        if (orderByVitamins(a.contains) == 1) {
          vitamin = "Vitamin A";
        } else if (orderByVitamins(a.contains) == 2) {
          vitamin = "Vitamin B";
        } else if (orderByVitamins(a.contains) == 3) {
          vitamin = "Vitamin C";
        } else if (orderByVitamins(a.contains) == 4) {
          vitamin = "Vitamin D";
        } else if (orderByVitamins(a.contains) == 5) {
          vitamin = "Vitamin K";
        }
        return (
          orderByVitamins(a.contains.replace(vitamin, "")) -
          orderByVitamins(b.contains.replace(vitamin, ""))
        );
      } else {
        return orderByVitamins(a.contains) - orderByVitamins(b.contains);
      }
    })
    .reduce(function (acc, item) {
      acc.push(item.name);
      return acc;
    }, []);
  return sortedItems;
}
module.exports = sortByVitamins;