function findAvailableItems(items) {
  const availableItems = items.reduce(function (acc, item) {
    if (item.available) {
      acc.push(item.name);
    }
    return acc;
  }, []);
  return availableItems;
}
module.exports = findAvailableItems;