function groupBasedOnVitamins(items) {
  const vitaminGroups = items.reduce(function (acc, item) {
    let vitamins = item.contains.split(", ");
    for (let vitamin of vitamins) {
      if (acc[vitamin]) {
        acc[vitamin].push(item.name);
      } else {
        acc[vitamin] = [item.name];
      }
    }
    return acc;
  }, {});
  return vitaminGroups;
}
module.exports = groupBasedOnVitamins;