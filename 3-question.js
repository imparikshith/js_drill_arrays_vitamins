function findItemsWithVitaminA(items) {
  const itemsWithVitaminA = items.reduce(function (acc, item) {
    if (item.contains.includes("Vitamin A")) {
      acc.push(item.name);
    }
    return acc;
  }, []);
  return itemsWithVitaminA;
}
module.exports = findItemsWithVitaminA;