function findItemsWithVitaminC(items) {
  const itemsWithVitaminC = items.reduce(function (acc, item) {
    if (item.contains == "Vitamin C") {
      acc.push(item.name);
    }
    return acc;
  }, []);
  return itemsWithVitaminC;
}
module.exports = findItemsWithVitaminC;